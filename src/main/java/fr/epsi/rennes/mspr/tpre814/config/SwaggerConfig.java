package fr.epsi.rennes.mspr.tpre814.config;

// springdoc-openapi-starter-webmvc-ui

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

// access to the Swagger UI at /swagger-ui.html
@EnableSpringDataWebSupport
@Configuration
@OpenAPIDefinition(
        info = @Info(
                title = "TPRE814 Product API",
                version = "0.4.4",
                description = "Documentation for the TPRE814 Product-Service API" +
                        " [Part of the TPRE814 Micro-Service project]:" +
                        " https://gitlab.com/tre814_i1/",
                contact = @Contact(
                        name = "TPRE814 Team",
                        email = ""),
                license = @License(
                        name = "Apache 2.0",
                        url = "https://www.apache.org/licenses/LICENSE-2.0.html"
                )
        )
)
//@SecurityScheme(
//        name="bearerAuth",
//        type= SecuritySchemeType.APIKEY,
//        scheme="bearer",
//        )
public class SwaggerConfig {
}
