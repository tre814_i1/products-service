package fr.epsi.rennes.mspr.tpre814.products.api;

import org.apache.kafka.common.TopicPartition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.listener.KafkaBackoffException;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ErrorHandlerDiffblueTest {

    private ErrorHandler errorHandler;
    @BeforeEach
    void setUp() {
        errorHandler = new ErrorHandler();
    }

    /**
     * Method under test:
     * {@link ErrorHandler#handleEnumConstantNotPresentException(EnumConstantNotPresentException)}
     */
    @Test
    void testHandleEnumConstantNotPresentException() {
        // Arrange
        Class<Enum> forNameResult = Enum.class;

        // Act
        ResponseEntity<String> actualHandleEnumConstantNotPresentExceptionResult = errorHandler
                .handleEnumConstantNotPresentException(new EnumConstantNotPresentException(forNameResult, "foo"));

        // Assert
        assertEquals("Invalid state : foo", actualHandleEnumConstantNotPresentExceptionResult.getBody());
        assertEquals(400, actualHandleEnumConstantNotPresentExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleEnumConstantNotPresentExceptionResult.getHeaders().isEmpty());
    }

    /**
     * Method under test: {@link ErrorHandler#handleException(Exception)}
     */
    @Test
    void testHandleException() {
        // Act
        ResponseEntity<String> actualHandleExceptionResult = errorHandler.handleException(new Exception("foo"));

        // Assert
        assertEquals("Internal server error", actualHandleExceptionResult.getBody());
        assertEquals(500, actualHandleExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleExceptionResult.getHeaders().isEmpty());
    }

    /**
     * Method under test:
     * {@link ErrorHandler#handleKafkaBackoffException(KafkaBackoffException)}
     */
    @Test
    void testHandleKafkaBackoffException() {
        // Act
        ResponseEntity<String> actualHandleKafkaBackoffExceptionResult = errorHandler.handleKafkaBackoffException(
                new KafkaBackoffException("An error occurred", new TopicPartition("Topic", 1), "42", 1L));

        // Assert
        assertEquals("Service Unavailable An error occurred", actualHandleKafkaBackoffExceptionResult.getBody());
        assertEquals(503, actualHandleKafkaBackoffExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleKafkaBackoffExceptionResult.getHeaders().isEmpty());
    }

    /**
     * Method under test: {@link ErrorHandler#handleNotFoundException(NoSuchElementException)}
     */
    @Test
    void testHandleNotFoundException() {
        // Act
        ResponseEntity<String> actualHandleNotFoundExceptionResult = errorHandler.handleNotFoundException(new NoSuchElementException("foo"));

        // Assert
        assertEquals(404, actualHandleNotFoundExceptionResult.getStatusCodeValue());
        assertTrue(actualHandleNotFoundExceptionResult.getHeaders().isEmpty());
    }
    
    /**
     * Method under test: {@link ErrorHandler#handleValidationError(IllegalArgumentException)}
     */
    @Test
    void testHandleValidationError() {
        // Act
        ResponseEntity<String> actualHandleValidationErrorResult = errorHandler.handleValidationError(new IllegalArgumentException("foo"));

        // Assert
        assertEquals("foo", actualHandleValidationErrorResult.getBody());
        assertEquals(400, actualHandleValidationErrorResult.getStatusCodeValue());
        assertTrue(actualHandleValidationErrorResult.getHeaders().isEmpty());
    }
}
