package fr.epsi.rennes.mspr.tpre814.products.api;

import fr.epsi.rennes.mspr.tpre814.products.services.ProductServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epsi.rennes.mspr.tpre814.products.services.interfaces.ProductService;
import fr.epsi.rennes.mspr.tpre814.shared.events.ProductRequest;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockOperation;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockRequest;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class StockEventListenerTest {
    private KafkaTemplate<UUID, String> template;
    private ProductService service;
    private StockEventListener stockEventListenerUnderTest;

    @BeforeEach
    public void setUp() {
        template = mock(KafkaTemplate.class);
        service = mock(ProductServiceImpl.class);
        stockEventListenerUnderTest = new StockEventListener(template, service);
    }

    @Test
    void testListenStockEvents_nok() {
        // Setup
        final StockRequest stockRequest = new StockRequest();
        stockRequest.setOp(StockOperation.SUB);
        stockRequest.setId(UUID.randomUUID());
        stockRequest.setPayload(null);

        // Run the test
        assertThrows(NullPointerException.class, () -> stockEventListenerUnderTest.listenStockEvents(stockRequest.toJson(), stockRequest.getId()));
    }

    @Test
    void testListenStockEvents_ok() {
        // Setup
        ProductRequest productRequest = new ProductRequest(UUID.randomUUID(), 1);
        final StockRequest stockRequest = new StockRequest();
        stockRequest.setOp(StockOperation.SUB);
        stockRequest.setId(UUID.randomUUID());
        stockRequest.setPayload(List.of(productRequest));

        // Run the test
        stockEventListenerUnderTest.listenStockEvents(stockRequest.toJson(), stockRequest.getId());

        // Verify the results
        verify(template).send(any(), any(), any());
    }
}