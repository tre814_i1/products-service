package fr.epsi.rennes.mspr.tpre814.products.database.entity;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ProductEntityTest {

    ProductEntity productEntity;

    @Test
    void validate_new_mustNotContainId() {
        productEntity = new ProductEntity();
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("name");
        productEntity.setPrice(1.0);
        productEntity.setStock(1);
        productEntity.setDescription("description");
        productEntity.setNew(true);
        productEntity.setActive(true);
        String error = assertThrows(IllegalStateException.class, () -> productEntity.validate()).getMessage();
        assertEquals("Product id must be null", error);
    }

    @Test
    void validate_name_mustNotBeEmpty() {
        productEntity = new ProductEntity();
        productEntity.setId(UUID.randomUUID());
        productEntity.setPrice(1.0);
        productEntity.setStock(1);
        productEntity.setDescription("description");
        productEntity.setNew(false);
        productEntity.setActive(true);
        String error = assertThrows(IllegalArgumentException.class, () -> productEntity.validate()).getMessage();
        assertEquals("Name is required", error);
    }

    @Test
    void validate_description_mustNotBeEmpty() {
        productEntity = new ProductEntity();
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("name");
        productEntity.setPrice(1.0);
        productEntity.setStock(1);
        productEntity.setNew(false);
        productEntity.setActive(true);
        String error = assertThrows(IllegalArgumentException.class, () -> productEntity.validate()).getMessage();
        assertEquals("Description is required", error);
    }

    @Test
    void validate_stock_mustBeGreaterThanOrEqualToZero() {
        productEntity = new ProductEntity();
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("name");
        productEntity.setDescription("description");
        productEntity.setPrice(1.0);
        productEntity.setStock(-1);
        productEntity.setNew(false);
        productEntity.setActive(true);
        String error = assertThrows(IllegalArgumentException.class, () -> productEntity.validate()).getMessage();
        assertEquals("Stock must be greater than or equal to 0", error);
    }

    @Test
    void validate_price_mustBeGreaterThanOrEqualToZero() {
        productEntity = new ProductEntity();
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("name");
        productEntity.setDescription("description");
        productEntity.setPrice(-1.0);
        productEntity.setStock(1);
        productEntity.setNew(false);
        productEntity.setActive(true);
        String error = assertThrows(IllegalArgumentException.class, () -> productEntity.validate()).getMessage();
        assertEquals("Price must be greater than or equal to 0", error);
    }

    @Test
    void validate_ok() {
        productEntity = new ProductEntity();
        productEntity.setName("name");
        productEntity.setDescription("description");
        productEntity.setPrice(1.0);
        productEntity.setStock(1);
        productEntity.setNew(true);
        productEntity.setActive(true);
        productEntity.validate();
    }

    @Test
    void testToString() {
        productEntity = new ProductEntity();
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("name");
        productEntity.setDescription("description");
        productEntity.setPrice(1.0);
        productEntity.setStock(1);
        productEntity.setNew(true);
        productEntity.setActive(true);
        assertEquals("ProductEntity(isNew=true, id=" + productEntity.getId() + ", name=name, description=description, stock=1, price=1.0, active=true)", productEntity.toString());
    }
}