package fr.epsi.rennes.mspr.tpre814.products.services;

import fr.epsi.rennes.mspr.tpre814.products.database.entity.ProductEntity;
import fr.epsi.rennes.mspr.tpre814.products.database.repository.ProductRepository;
import fr.epsi.rennes.mspr.tpre814.products.errors.NotActiveException;
import fr.epsi.rennes.mspr.tpre814.products.services.interfaces.ProductService;
import fr.epsi.rennes.mspr.tpre814.products.utils.ProductsMapper;
import fr.epsi.rennes.mspr.tpre814.shared.events.ProductRequest;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockRequest;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import jakarta.persistence.EntityNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.function.ObjIntConsumer;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository repository;

    /**
     * {@inheritDoc}
     */
    @Override
    public Product create(@NonNull ProductEntity entity) {
        entity.setNew(true);
        log.info("Creating product: {}", entity);
        entity.validate();
        entity.setId(UUID.randomUUID());
        entity.setActive(true);
        log.info("Saving: {}", entity);
        return ProductsMapper.toProduct(repository.save(entity));
    }

    /**
     * Create all products
     *
     * @param entities list of products to create
     * @return list of products created
     */
    public List<Product> createAll(List<ProductEntity> entities) {
        log.info("Creating {} products...", entities.size());
        return entities.stream().map(this::create).toList();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductEntity get(UUID id) {
        ProductEntity entity = getEntity(id);
        if (!entity.isActive()) {
            throw new NotActiveException("ProductEntity not active for id: " + id);
        }
        return entity;
    }

    private ProductEntity getEntity(UUID id) {
        return repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("ProductEntity not found with id: " + id));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProductEntity> all() {
        return repository.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductEntity update(ProductEntity entity) {
        entity.setNew(false);
        ProductEntity toUpdate = get(entity.getId());
        log.debug("Updating product: {}", toUpdate);
        if (entity.getName() != null) {
            log.info("Updating name for product: {} to {}", entity.getId(), entity.getName());
            toUpdate.setName(entity.getName());
        }
        if (entity.getDescription() != null) {
            log.info("Updating description for product: {} to {}", entity.getId(), entity.getDescription());
            toUpdate.setDescription(entity.getDescription());
        }
        if (entity.getStock() != null && entity.getStock() != 0) {
            log.info("Updating stock for product: {} to {}", entity.getId(), entity.getStock());
            toUpdate.setStock(entity.getStock());
        }
        if (entity.getPrice() != null && entity.getPrice() != 0) {
            log.info("Updating price for product: {} to {}", entity.getId(), entity.getPrice());
            toUpdate.setPrice(entity.getPrice());
        }
        toUpdate.validate();
        log.debug("Saving: {}", toUpdate);
        return repository.save(toUpdate);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(UUID id) {
        ProductEntity entity = get(id);
        entity.setActive(false);
        repository.save(entity);
    }

    /**
     * depending on the operation, add or remove stock from the products
     *
     * @param entities list of products to update
     * @param request  the request received from the broker
     */
    @Override
    @Async
    public void handleStockRequest(List<ProductEntity> entities, StockRequest request) {
        log.debug("Handling request");

        List<ProductEntity> updatedEntities = switch (request.getOp()) {
            case ADD -> updateStock(entities, request.getPayload(), this::addStock);
            case SUB -> updateStock(entities, request.getPayload(), this::removeStock);
        };

        repository.saveAll(updatedEntities);
    }

    /**
     * Find all products by their ids
     *
     * @param list list of ids
     * @return list of products
     */
    @Override
    public List<ProductEntity> findAllById(List<UUID> list) {
        return repository.findAllById(list);
    }

    /**
     * Update the stock of the products
     *
     * @param entities     list of products
     * @param payload      list of products to update
     * @param stockUpdater the function to update the stock
     * @return the updated list of products
     */
    List<ProductEntity> updateStock(List<ProductEntity> entities, List<ProductRequest> payload, ObjIntConsumer<ProductEntity> stockUpdater) {
        payload.forEach(request -> {
            ProductEntity entity = findEntityById(entities, request.id());
            log.info("Updating stock for product: {}", entity.getId());
            stockUpdater.accept(entity, request.quantity());
        });
        return entities;
    }

    /**
     * Add stock to a product
     *
     * @param entity   the product
     * @param quantity the quantity to add
     */
    void addStock(ProductEntity entity, int quantity) {
        log.debug("Adding stock to product: {}", entity.getId());
        if (quantity < 0) {
            throw new IllegalArgumentException("Negative stock quantity for product: %s"
                    .formatted(entity.getId()));
        }
        entity.setStock(entity.getStock() + quantity);
    }

    void removeStock(ProductEntity entity, int quantity) {
        log.debug("Removing stock from product: {}", entity.getId());
        if (entity.getStock() <= quantity) {
            log.warn("Stock shortage for product: {}", entity.getId());
            // TODO: send a message to restock
        }
        log.debug("before: {}", entity.getStock());
        entity.setStock(entity.getStock() - Math.min(quantity, entity.getStock()));
        log.debug("after: {}", entity.getStock());
    }

    private ProductEntity findEntityById(List<ProductEntity> entities, UUID id) {
        return entities.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("ProductEntity not found with id: " + id));
    }

}