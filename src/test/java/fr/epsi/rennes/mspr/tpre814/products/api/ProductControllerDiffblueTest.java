package fr.epsi.rennes.mspr.tpre814.products.api;

import fr.epsi.rennes.mspr.tpre814.products.database.entity.ProductEntity;
import fr.epsi.rennes.mspr.tpre814.products.database.repository.ProductRepository;
import fr.epsi.rennes.mspr.tpre814.products.services.ProductServiceImpl;
import fr.epsi.rennes.mspr.tpre814.products.utils.ProductsMapper;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import fr.epsi.rennes.mspr.tpre814.shared.models.ProductDetails;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

class ProductControllerDiffblueTest {
    /**
     * Method under test: {@link ProductController#create(Product)}}
     */
   @Test
    void testCreate() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(false);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setName("Name");
        productEntity.setNew(true);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        ProductRepository repository = mock(ProductRepository.class);
        when(repository.save(Mockito.any())).thenReturn(productEntity);
        ProductController productController = new ProductController(new ProductServiceImpl(repository));

        Product product = ProductsMapper.toProduct(productEntity);
        // Act
        Product actualCreateResult = productController.create(product);

        // Assert
        verify(repository).save(isA(ProductEntity.class));
        assertEquals("Name", actualCreateResult.getName());
        ProductDetails details = actualCreateResult.getDetails();
        assertEquals("Name", details.getName());
        assertEquals("The characteristics of someone or something", details.getDescription());
        assertEquals(1, actualCreateResult.getStock());
        assertEquals(10.0d, details.getPrice());
    }

    /**
     * Method under test: {@link ProductController#createAll(List)}
     */
    @Test
    void testCreateAll() {
        // Arrange
        ProductController productController = new ProductController(new ProductServiceImpl(mock(ProductRepository.class)));

        // Act and Assert
        assertTrue(productController.createAll(new ArrayList<>()).isEmpty());
    }

    /**
     * Method under test: {@link ProductController#all()}
     */
    @Test
    void testAll() {
        // Arrange
        ProductRepository repository = mock(ProductRepository.class);
        ArrayList<ProductEntity> productEntityList = new ArrayList<>();
        when(repository.findAll()).thenReturn(productEntityList);

        // Act
        List<Product> actualAllResult = (new ProductController(new ProductServiceImpl(repository))).all();

        // Assert
        verify(repository).findAll();
        assertTrue(actualAllResult.isEmpty());
        assertSame(ProductsMapper.toProduct(productEntityList), actualAllResult);
    }

    /**
     * Method under test: {@link ProductController#update(Product)}
     */
    @Test
    void testUpdate() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("Name");
        productEntity.setNew(false);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        ProductRepository repository = mock(ProductRepository.class);
        when(repository.save(Mockito.any())).thenReturn(productEntity);
        ProductController productController = new ProductController(new ProductServiceImpl(repository));

        ProductEntity entity = new ProductEntity();
        entity.setActive(true);
        entity.setDescription("The characteristics of someone or something");
        entity.setId(UUID.randomUUID());
        entity.setName("Name");
        entity.setNew(false);
        entity.setPrice(10.0d);
        entity.setStock(1);

        when(repository.findById(Mockito.any())).thenReturn(Optional.of(entity));
        when(repository.save(Mockito.any())).thenReturn(productEntity);
        // Act
        Product actualUpdateResult = productController.update(ProductsMapper.toProduct(entity));

        // Assert
        verify(repository).save(isA(ProductEntity.class));
        assertThat(actualUpdateResult)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(ProductsMapper.toProduct(productEntity));
    }

    /**
     * Method under test: {@link ProductController#delete(UUID)}
     */
    @Test
    void testDelete() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("Name");
        productEntity.setNew(true);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        Optional<ProductEntity> ofResult = Optional.of(productEntity);

        ProductEntity productEntity2 = new ProductEntity();
        productEntity2.setActive(true);
        productEntity2.setDescription("The characteristics of someone or something");
        productEntity2.setId(UUID.randomUUID());
        productEntity2.setName("Name");
        productEntity2.setNew(true);
        productEntity2.setPrice(10.0d);
        productEntity2.setStock(1);
        ProductRepository repository = mock(ProductRepository.class);
        when(repository.save(Mockito.any())).thenReturn(productEntity2);
        when(repository.findById(Mockito.any())).thenReturn(ofResult);
        ProductController productController = new ProductController(new ProductServiceImpl(repository));

        // Act
        productController.delete(UUID.randomUUID());

        // Assert
        verify(repository).findById(isA(UUID.class));
        verify(repository).save(isA(ProductEntity.class));
    }

    /**
     * Method under test: {@link ProductController#get(UUID)}
     */
    @Test
    void testGet() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("Name");
        productEntity.setNew(true);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        Optional<ProductEntity> ofResult = Optional.of(productEntity);
        ProductRepository repository = mock(ProductRepository.class);
        when(repository.findById(Mockito.any())).thenReturn(ofResult);
        ProductController productController = new ProductController(new ProductServiceImpl(repository));

        // Act
        Product actualGetResult = productController.get(UUID.randomUUID());

        // Assert
        verify(repository).findById(isA(UUID.class));
        assertThat(actualGetResult)
                .usingRecursiveComparison()
                .isEqualTo(ProductsMapper.toProduct(productEntity));
    }
}
