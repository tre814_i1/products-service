package fr.epsi.rennes.mspr.tpre814.products.services;

import fr.epsi.rennes.mspr.tpre814.products.database.entity.ProductEntity;
import fr.epsi.rennes.mspr.tpre814.products.database.repository.ProductRepository;
import fr.epsi.rennes.mspr.tpre814.products.errors.NotActiveException;
import fr.epsi.rennes.mspr.tpre814.shared.events.ProductRequest;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockOperation;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockRequest;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import fr.epsi.rennes.mspr.tpre814.shared.models.ProductDetails;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.ObjIntConsumer;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

class ProductServiceImplDiffblueTest {

    private ProductRepository repository;
    private ProductServiceImpl productServiceImpl;

    @BeforeEach
    void setUp() {
        repository = mock(ProductRepository.class);
        productServiceImpl = new ProductServiceImpl(repository);
    }

    /**
     * Method under test: {@link ProductServiceImpl#create(ProductEntity)}
     */
    @Test
    void testCreate() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("Name");
        productEntity.setNew(true);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        when(repository.save(Mockito.any())).thenReturn(productEntity);

        ProductEntity entity = new ProductEntity();
        entity.setActive(true);
        entity.setDescription("The characteristics of someone or something");
        entity.setId(null);
        entity.setName("Name");
        entity.setNew(true);
        entity.setPrice(10.0d);
        entity.setStock(1);

        // Act
        Product actualCreateResult = productServiceImpl.create(entity);

        // Assert
        verify(repository).save(isA(ProductEntity.class));
        assertEquals("Name", actualCreateResult.getName());
        ProductDetails details = actualCreateResult.getDetails();
        assertEquals("Name", details.getName());
        assertEquals("The characteristics of someone or something", details.getDescription());
        assertEquals(1, actualCreateResult.getStock());
        assertEquals(10.0d, details.getPrice());
        assertEquals(2, entity.getId().variant());
        assertEquals(2, actualCreateResult.getId().variant());
        assertTrue(entity.isNew());
    }

    /**
     * Method under test: {@link ProductServiceImpl#create(ProductEntity)}
     */
    @Test
    void testCreate2() {
        // Arrange
        when(repository.save(Mockito.any()))
                .thenThrow(new MatchException("0123456789ABCDEF", new Throwable()));

        ProductEntity entity = new ProductEntity();
        entity.setActive(true);
        entity.setDescription("The characteristics of someone or something");
        entity.setId(null);
        entity.setName("Name");
        entity.setNew(true);
        entity.setPrice(10.0d);
        entity.setStock(1);

        // Act and Assert
        assertThrows(MatchException.class, () -> productServiceImpl.create(entity));
        verify(repository).save(isA(ProductEntity.class));
    }

    /**
     * Method under test: {@link ProductServiceImpl#createAll(List)}
     */
    @Test
    void testCreateAll() {
        // Act and Assert
        assertTrue(productServiceImpl.createAll(new ArrayList<>()).isEmpty());
    }

    /**
     * Method under test: {@link ProductServiceImpl#createAll(List)}
     */
    @Test
    void testCreateAll2() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("Name");
        productEntity.setNew(true);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        when(repository.save(Mockito.any())).thenReturn(productEntity);

        ProductEntity productEntity2 = new ProductEntity();
        productEntity2.setActive(true);
        productEntity2.setPrice(0.0d);
        productEntity2.setName("Creating product: {}");
        productEntity2.setDescription("The characteristics of someone or something");
        productEntity2.setNew(false);
        productEntity2.setId(null);
        productEntity2.setStock(0);

        ArrayList<ProductEntity> entities = new ArrayList<>();
        entities.add(productEntity2);

        // Act
        List<Product> actualCreateAllResult = productServiceImpl.createAll(entities);

        // Assert
        verify(repository).save(isA(ProductEntity.class));
        assertEquals(1, actualCreateAllResult.size());
        Product getResult = actualCreateAllResult.get(0);
        assertEquals("Name", getResult.getName());
        ProductDetails details = getResult.getDetails();
        assertEquals("Name", details.getName());
        assertEquals("The characteristics of someone or something", details.getDescription());
        assertEquals(1, getResult.getStock());
        assertEquals(10.0d, details.getPrice());
        assertEquals(2, getResult.getId().variant());
    }

    /**
     * Method under test: {@link ProductServiceImpl#createAll(List)}
     */
    @Test
    void testCreateAll3() {
        // Arrange
        when(repository.save(Mockito.any()))
                .thenThrow(new MatchException("0123456789ABCDEF", new Throwable()));

        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setPrice(0.0d);
        productEntity.setName("Creating product: {}");
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setNew(false);
        productEntity.setId(null);
        productEntity.setStock(0);

        ArrayList<ProductEntity> entities = new ArrayList<>();
        entities.add(productEntity);

        // Act and Assert
        assertThrows(MatchException.class, () -> productServiceImpl.createAll(entities));
        verify(repository).save(isA(ProductEntity.class));
    }

    /**
     * Method under test: {@link ProductServiceImpl#get(UUID)}
     */
    @Test
    void testGet() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("Name");
        productEntity.setNew(true);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        Optional<ProductEntity> ofResult = Optional.of(productEntity);
        when(repository.findById(Mockito.any())).thenReturn(ofResult);

        // Act
        ProductEntity actualGetResult = productServiceImpl.get(UUID.randomUUID());

        // Assert
        verify(repository).findById(isA(UUID.class));
        assertSame(productEntity, actualGetResult);
    }

    /**
     * Method under test: {@link ProductServiceImpl#get(UUID)}
     */
    @Test
    void testGet2() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(false);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("Name");
        productEntity.setNew(true);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        Optional<ProductEntity> ofResult = Optional.of(productEntity);
        when(repository.findById(Mockito.any())).thenReturn(ofResult);

        // Act an Assert
        assertThrows(NotActiveException.class, () -> productServiceImpl.get(UUID.randomUUID()));

        verify(repository).findById(isA(UUID.class));
    }

    /**
     * Method under test: {@link ProductServiceImpl#get(UUID)}
     */
    @Test
    void testGet3() {
        // Arrange
        Optional<ProductEntity> emptyResult = Optional.empty();
        when(repository.findById(Mockito.any())).thenReturn(emptyResult);

        // Act and Assert
        assertThrows(EntityNotFoundException.class, () -> productServiceImpl.get(UUID.randomUUID()));
        verify(repository).findById(isA(UUID.class));
    }

    /**
     * Method under test: {@link ProductServiceImpl#get(UUID)}
     */
    @Test
    void testGet4() {
        // Arrange
        when(repository.findById(Mockito.any())).thenThrow(new MatchException("0123456789ABCDEF", new Throwable()));

        // Act and Assert
        assertThrows(MatchException.class, () -> productServiceImpl.get(UUID.randomUUID()));
        verify(repository).findById(isA(UUID.class));
    }

    /**
     * Method under test: {@link ProductServiceImpl#all()}
     */
    @Test
    void testAll() {
        // Arrange
        ArrayList<ProductEntity> productEntityList = new ArrayList<>();
        when(repository.findAll()).thenReturn(productEntityList);

        // Act
        List<ProductEntity> actualAllResult = (new ProductServiceImpl(repository)).all();

        // Assert
        verify(repository).findAll();
        assertTrue(actualAllResult.isEmpty());
        assertSame(productEntityList, actualAllResult);
    }

    /**
     * Method under test: {@link ProductServiceImpl#all()}
     */
    @Test
    void testAll2() {
        // Arrange
        when(repository.findAll()).thenThrow(new MatchException("0123456789ABCDEF", new Throwable()));

        // Act and Assert
        assertThrows(MatchException.class, () -> (new ProductServiceImpl(repository)).all());
        verify(repository).findAll();
    }

    /**
     * Method under test: {@link ProductServiceImpl#update(ProductEntity)}
     */
    @Test
    void testUpdate() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("Name");
        productEntity.setNew(true);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        when(repository.save(Mockito.any())).thenReturn(productEntity);


        when(repository.findById(Mockito.any())).thenReturn(Optional.of(productEntity));
        when(repository.save(Mockito.any())).thenReturn(productEntity);

        // Act
        ProductEntity actualUpdateResult = productServiceImpl.update(productEntity);

        // Assert
        verify(repository).save(isA(ProductEntity.class));
        assertSame(productEntity, actualUpdateResult);
    }

    /**
     * Method under test: {@link ProductServiceImpl#update(ProductEntity)}
     */
    @Test
    void testUpdate2() {
        // Arrange
        when(repository.save(Mockito.any()))
                .thenThrow(new MatchException("0123456789ABCDEF", new Throwable()));

        ProductEntity entity = mock(ProductEntity.class);
        when(entity.getId()).thenReturn(UUID.randomUUID());
        when(entity.isActive()).thenReturn(true);
        when(entity.getDescription()).thenReturn("The characteristics of someone or something");
        when(entity.getName()).thenReturn("Name");
        when(entity.isNew()).thenReturn(false);
        when(entity.getPrice()).thenReturn(10.0d);
        when(entity.getStock()).thenReturn(1);
        when(repository.findById(Mockito.any())).thenReturn(Optional.of(entity));

        // Act and Assert
        assertThrows(MatchException.class, () -> productServiceImpl.update(entity));
        verify(repository).save(isA(ProductEntity.class));
        verify(entity, atLeast(1)).getId();
        verify(entity).isActive();
        verify(entity, atLeast(1)).getDescription();
        verify(entity, atLeast(1)).getName();
        verify(entity, atLeast(1)).getPrice();
        verify(entity, atLeast(1)).validate();
    }

    /**
     * Method under test: {@link ProductServiceImpl#update(ProductEntity)}
     */
    @Test
    void testUpdate_missingDescription() {
        // Arrange
        ProductEntity entity = mock(ProductEntity.class);
        when(entity.getId()).thenReturn(UUID.randomUUID());
        when(entity.isActive()).thenReturn(true);
        when(entity.getName()).thenReturn("Name");
        when(entity.isNew()).thenReturn(false);
        when(entity.getDescription()).thenReturn("");
        when(entity.getPrice()).thenReturn(10.0d);
        when(entity.getStock()).thenReturn(1);
        when(repository.findById(Mockito.any())).thenReturn(Optional.of(entity));

        // Act and Assert
        productServiceImpl.update(entity);
        verify(entity, times(1)).validate();
        verify(repository).save(isA(ProductEntity.class));
    }

    /**
     * Method under test: {@link ProductServiceImpl#delete(UUID)}
     */
    @Test
    void testDelete() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("Name");
        productEntity.setNew(true);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        Optional<ProductEntity> ofResult = Optional.of(productEntity);

        ProductEntity productEntity2 = new ProductEntity();
        productEntity2.setActive(true);
        productEntity2.setDescription("The characteristics of someone or something");
        productEntity2.setId(UUID.randomUUID());
        productEntity2.setName("Name");
        productEntity2.setNew(true);
        productEntity2.setPrice(10.0d);
        productEntity2.setStock(1);
        when(repository.save(Mockito.any())).thenReturn(productEntity2);
        when(repository.findById(Mockito.any())).thenReturn(ofResult);

        // Act
        productServiceImpl.delete(UUID.randomUUID());

        // Assert
        verify(repository).findById(isA(UUID.class));
        verify(repository).save(isA(ProductEntity.class));
    }

    /**
     * Method under test: {@link ProductServiceImpl#delete(UUID)}
     */
    @Test
    void testDelete2() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("Name");
        productEntity.setNew(true);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        Optional<ProductEntity> ofResult = Optional.of(productEntity);
        when(repository.save(Mockito.any()))
                .thenThrow(new MatchException("0123456789ABCDEF", new Throwable()));
        when(repository.findById(Mockito.any())).thenReturn(ofResult);

        // Act and Assert
        assertThrows(MatchException.class, () -> productServiceImpl.delete(UUID.randomUUID()));
        verify(repository).findById(isA(UUID.class));
        verify(repository).save(isA(ProductEntity.class));
    }

    /**
     * Method under test: {@link ProductServiceImpl#delete(UUID)}
     */
    @Test
    void testDelete3() {
        // Arrange
        Optional<ProductEntity> emptyResult = Optional.empty();
        when(repository.findById(Mockito.any())).thenReturn(emptyResult);

        // Act and Assert
        assertThrows(EntityNotFoundException.class, () -> productServiceImpl.delete(UUID.randomUUID()));
        verify(repository).findById(isA(UUID.class));
    }

    /**
     * Method under test:
     * {@link ProductServiceImpl#handleStockRequest(List, StockRequest)}
     */
    @Test
    void testHandleStockRequest() {
        // Arrange
        when(repository.saveAll(Mockito.any())).thenReturn(new ArrayList<>());
        ArrayList<ProductEntity> entities = new ArrayList<>();
        UUID id = UUID.randomUUID();

        StockRequest request = new StockRequest(id, new ArrayList<>());
        request.setOp(StockOperation.ADD);

        // Act
        productServiceImpl.handleStockRequest(entities, request);

        // Assert
        verify(repository).saveAll(isA(Iterable.class));
    }

    /**
     * Method under test:
     * {@link ProductServiceImpl#handleStockRequest(List, StockRequest)}
     */
    @Test
    void testHandleStockRequest2() {
        // Arrange
        ArrayList<ProductEntity> entities = new ArrayList<>();

        ArrayList<ProductRequest> payload = new ArrayList<>();
        payload.add(new ProductRequest(UUID.randomUUID(), 1));

        StockRequest request = new StockRequest(UUID.randomUUID(), payload);
        request.setOp(StockOperation.ADD);

        // Act and Assert
        assertThrows(EntityNotFoundException.class, () -> productServiceImpl.handleStockRequest(entities, request));
    }

    /**
     * Method under test:
     * {@link ProductServiceImpl#handleStockRequest(List, StockRequest)}
     */
    @Test
    void testHandleStockRequest3() {
        // Arrange
        when(repository.saveAll(Mockito.any())).thenReturn(new ArrayList<>());
        ArrayList<ProductEntity> entities = new ArrayList<>();
        UUID id = UUID.randomUUID();

        StockRequest request = new StockRequest(id, new ArrayList<>());
        request.setOp(StockOperation.SUB);

        // Act
        productServiceImpl.handleStockRequest(entities, request);

        // Assert
        verify(repository).saveAll(isA(Iterable.class));
    }

    /**
     * Method under test:
     * {@link ProductServiceImpl#handleStockRequest(List, StockRequest)}
     */
    @Test
    void testHandleStockRequest4() {
        // Arrange
        when(repository.saveAll(Mockito.any()))
                .thenThrow(new MatchException("0123456789ABCDEF", new Throwable()));
        ArrayList<ProductEntity> entities = new ArrayList<>();
        UUID id = UUID.randomUUID();

        StockRequest request = new StockRequest(id, new ArrayList<>());
        request.setOp(StockOperation.ADD);

        // Act and Assert
        assertThrows(MatchException.class, () -> productServiceImpl.handleStockRequest(entities, request));
        verify(repository).saveAll(isA(Iterable.class));
    }

    /**
     * Method under test:
     * {@link ProductServiceImpl#handleStockRequest(List, StockRequest)}
     */
    @Test
    void testHandleStockRequest5() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("Description");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("be");
        productEntity.setNew(true);
        productEntity.setPrice(0.5d);
        productEntity.setStock(0);

        ArrayList<ProductEntity> entities = new ArrayList<>();
        entities.add(productEntity);

        ArrayList<ProductRequest> payload = new ArrayList<>();
        payload.add(new ProductRequest(UUID.randomUUID(), 1));

        StockRequest request = new StockRequest(UUID.randomUUID(), payload);
        request.setOp(StockOperation.ADD);

        // Act and Assert
        assertThrows(EntityNotFoundException.class, () -> productServiceImpl.handleStockRequest(entities, request));
    }

    /**
     * Method under test:
     * {@link ProductServiceImpl#handleStockRequest(List, StockRequest)}
     */
    @Test
    void testHandleStockRequest6() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("Description");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("be");
        productEntity.setNew(true);
        productEntity.setPrice(0.5d);
        productEntity.setStock(0);

        ProductEntity productEntity2 = new ProductEntity();
        productEntity2.setActive(false);
        productEntity2.setDescription("The characteristics of someone or something");
        productEntity2.setId(UUID.randomUUID());
        productEntity2.setName("Handling request");
        productEntity2.setNew(false);
        productEntity2.setPrice(10.0d);
        productEntity2.setStock(1);

        ArrayList<ProductEntity> entities = new ArrayList<>();
        entities.add(productEntity2);
        entities.add(productEntity);

        ArrayList<ProductRequest> payload = new ArrayList<>();
        payload.add(new ProductRequest(UUID.randomUUID(), 1));

        StockRequest request = new StockRequest(UUID.randomUUID(), payload);
        request.setOp(StockOperation.ADD);

        // Act and Assert
        assertThrows(EntityNotFoundException.class, () -> productServiceImpl.handleStockRequest(entities, request));
    }

    /**
     * Method under test: {@link ProductServiceImpl#findAllById(List)}
     */
    @Test
    void testFindAllById() {
        // Arrange
        ArrayList<ProductEntity> productEntityList = new ArrayList<>();
        when(repository.findAllById(Mockito.any())).thenReturn(productEntityList);

        // Act
        List<ProductEntity> actualFindAllByIdResult = productServiceImpl.findAllById(new ArrayList<>());

        // Assert
        verify(repository).findAllById(isA(Iterable.class));
        assertTrue(actualFindAllByIdResult.isEmpty());
        assertSame(productEntityList, actualFindAllByIdResult);
    }

    /**
     * Method under test: {@link ProductServiceImpl#findAllById(List)}
     */
    @Test
    void testFindAllById2() {
        // Arrange
        ArrayList<ProductEntity> productEntityList = new ArrayList<>();
        when(repository.findAllById(Mockito.any())).thenReturn(productEntityList);

        ArrayList<UUID> list = new ArrayList<>();
        list.add(UUID.randomUUID());

        // Act
        List<ProductEntity> actualFindAllByIdResult = productServiceImpl.findAllById(list);

        // Assert
        verify(repository).findAllById(isA(Iterable.class));
        assertTrue(actualFindAllByIdResult.isEmpty());
        assertSame(productEntityList, actualFindAllByIdResult);
    }

    /**
     * Method under test: {@link ProductServiceImpl#findAllById(List)}
     */
    @Test
    void testFindAllById3() {
        // Arrange
        ArrayList<ProductEntity> productEntityList = new ArrayList<>();
        when(repository.findAllById(Mockito.any())).thenReturn(productEntityList);

        ArrayList<UUID> list = new ArrayList<>();
        list.add(UUID.randomUUID());
        list.add(UUID.randomUUID());

        // Act
        List<ProductEntity> actualFindAllByIdResult = productServiceImpl.findAllById(list);

        // Assert
        verify(repository).findAllById(isA(Iterable.class));
        assertTrue(actualFindAllByIdResult.isEmpty());
        assertSame(productEntityList, actualFindAllByIdResult);
    }

    /**
     * Method under test: {@link ProductServiceImpl#findAllById(List)}
     */
    @Test
    void testFindAllById4() {
        // Arrange
        when(repository.findAllById(Mockito.any()))
                .thenThrow(new MatchException("0123456789ABCDEF", new Throwable()));

        // Act and Assert
        assertThrows(MatchException.class, () -> productServiceImpl.findAllById(new ArrayList<>()));
        verify(repository).findAllById(isA(Iterable.class));
    }


    /**
     * Method under test: {@link ProductServiceImpl#updateStock(List, List, ObjIntConsumer)}
     */
    @Test
    void updateStock_nok() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("Name");
        productEntity.setNew(true);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);

        ArrayList<ProductEntity> entities = new ArrayList<>();
        entities.add(productEntity);

        ArrayList<ProductRequest> payload = new ArrayList<>();
        payload.add(new ProductRequest(UUID.randomUUID(), 1));

        ObjIntConsumer<ProductEntity> stockUpdater = ProductEntity::setStock;

        // Act and Assert
        assertThrows(EntityNotFoundException.class, () -> productServiceImpl.updateStock(entities, payload, stockUpdater));

        assertEquals(1, productEntity.getStock());
    }

    /**
     * Method under test: {@link ProductServiceImpl#updateStock(List, List, ObjIntConsumer)}
     */
    @Test
    void updateStock_add_nok() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("Name");
        productEntity.setNew(true);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);

        ArrayList<ProductEntity> entities = new ArrayList<>();
        entities.add(productEntity);

        ArrayList<ProductRequest> payload = new ArrayList<>();
        payload.add(new ProductRequest(UUID.randomUUID(), 1));

        ObjIntConsumer<ProductEntity> stockUpdater = productServiceImpl::addStock;

        // Act and Assert
        assertThrows(EntityNotFoundException.class, () -> productServiceImpl.updateStock(entities, payload, stockUpdater));

        assertEquals(1, productEntity.getStock());
    }

    /**
     * Method under test: {@link ProductServiceImpl#updateStock(List, List, ObjIntConsumer)}
     */
    @Test
    void updateStock_sub_nok() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("Name");
        productEntity.setNew(true);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);

        ArrayList<ProductEntity> entities = new ArrayList<>();
        entities.add(productEntity);

        ArrayList<ProductRequest> payload = new ArrayList<>();
        payload.add(new ProductRequest(UUID.randomUUID(), 1));

        ObjIntConsumer<ProductEntity> stockUpdater = productServiceImpl::removeStock;

        // Act and Assert
        assertThrows(EntityNotFoundException.class, () -> productServiceImpl.updateStock(entities, payload, stockUpdater));

        assertEquals(1, productEntity.getStock());
    }

    /**
     * Method under test: {@link ProductServiceImpl#updateStock(List, List, ObjIntConsumer)}
     */
    @Test
    void updateStock_ok() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setActive(true);
        productEntity.setDescription("The characteristics of someone or something");
        productEntity.setId(UUID.randomUUID());
        productEntity.setName("Name");
        productEntity.setNew(false);
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);

        ArrayList<ProductEntity> entities = new ArrayList<>();
        entities.add(productEntity);

        ArrayList<ProductRequest> payload = new ArrayList<>();
        payload.add(new ProductRequest(productEntity.getId(), 1));

        ObjIntConsumer<ProductEntity> stockUpdater = mock(ObjIntConsumer.class);


        // Act and Assert
        List<ProductEntity> result = assertDoesNotThrow(() -> productServiceImpl.updateStock(entities, payload, stockUpdater));

        assertEquals(1, result.size());
        assertEquals(1, result.getFirst().getStock());
        assertEquals(1, productEntity.getStock());
        verify(repository, times(0)).saveAll(any());
        verify(stockUpdater).accept(isA(ProductEntity.class), anyInt());
    }

    /**
     * Method under test: {@link ProductServiceImpl#addStock(ProductEntity, int)}
     */
    @Test
    void addStock() {
        // Arrange
        ProductEntity entity = new ProductEntity();
        entity.setActive(true);
        entity.setDescription("The characteristics of someone or something");
        entity.setId(UUID.randomUUID());
        entity.setName("Name");
        entity.setNew(true);
        entity.setPrice(10.0d);
        entity.setStock(1);

        // Act
        productServiceImpl.addStock(entity, 1);

        // Assert
        assertEquals(2, entity.getStock());
    }

    /**
     * Method under test: {@link ProductServiceImpl#addStock(ProductEntity, int)}
     */
    @Test
    void addStock_negativeValue_nok() {
        // Arrange
        ProductEntity entity = new ProductEntity();
        entity.setActive(true);
        entity.setDescription("The characteristics of someone or something");
        entity.setId(UUID.randomUUID());
        entity.setName("Name");
        entity.setNew(true);
        entity.setPrice(10.0d);
        entity.setStock(1);

        // Act
        assertThrows(IllegalArgumentException.class, () -> productServiceImpl.addStock(entity, -1));

        // Assert
        assertEquals(1, entity.getStock());
    }

    /**
     * Method under test: {@link ProductServiceImpl#removeStock(ProductEntity, int)}
     */
    @Test
    void removeStock() {
        // Arrange
        ProductEntity entity = new ProductEntity();
        entity.setActive(true);
        entity.setDescription("The characteristics of someone or something");
        entity.setId(UUID.randomUUID());
        entity.setName("Name");
        entity.setNew(true);
        entity.setPrice(10.0d);
        entity.setStock(1);

        // Act
        productServiceImpl.removeStock(entity, 1);

        // Assert
        assertEquals(0, entity.getStock());
    }
}
