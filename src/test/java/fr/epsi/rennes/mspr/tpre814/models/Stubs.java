package fr.epsi.rennes.mspr.tpre814.models;

import fr.epsi.rennes.mspr.tpre814.products.database.entity.ProductEntity;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import fr.epsi.rennes.mspr.tpre814.shared.models.ProductDetails;

import java.util.UUID;

public class Stubs {

    public static ProductEntity newTestProductEntity() {
        ProductEntity entity = new ProductEntity();
        entity.setId(UUID.randomUUID());
        entity.setName("Test");
        entity.setStock(10);
        entity.setPrice(10.0);
        entity.setDescription("Test description");
        return entity;
    }

    public static Product newTestProduct() {
        Product product = new Product();
        product.setId(UUID.randomUUID());
        product.setName("Test");
        product.setStock(10);
        ProductDetails details = new ProductDetails();
        details.setPrice(10.0);
        details.setName("Test");
        details.setDescription("Test description");
        product.setDetails(details);
        return product;
    }
}
