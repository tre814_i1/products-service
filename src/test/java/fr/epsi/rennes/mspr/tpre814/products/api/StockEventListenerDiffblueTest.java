package fr.epsi.rennes.mspr.tpre814.products.api;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import fr.epsi.rennes.mspr.tpre814.products.database.repository.ProductRepository;
import fr.epsi.rennes.mspr.tpre814.products.services.ProductServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.kafka.clients.producer.MockProducer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.common.serialization.UUIDSerializer;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

class StockEventListenerDiffblueTest {
    /**
     * Method under test:
     * {@link StockEventListener#listenProductRequest(String, UUID)}
     */
    @Test
    void testListenProductRequest() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        UUIDSerializer keySerializer = new UUIDSerializer();
        when(producerFactory.createProducer()).thenReturn(new MockProducer<>(true, keySerializer, new StringSerializer()));
        when(producerFactory.transactionCapable()).thenReturn(false);
        KafkaTemplate<UUID, String> anyTemplate = new KafkaTemplate<>(producerFactory);
        ProductRepository repository = mock(ProductRepository.class);
        when(repository.findAllById(Mockito.<Iterable<UUID>>any())).thenReturn(new ArrayList<>());
        StockEventListener stockEventListener = new StockEventListener(anyTemplate, new ProductServiceImpl(repository));

        // Act
        stockEventListener.listenProductRequest("42", UUID.randomUUID());

        // Assert
        verify(repository).findAllById(isA(Iterable.class));
        verify(producerFactory).createProducer();
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test:
     * {@link StockEventListener#listenProductRequest(String, UUID)}
     */
    @Test
    void testListenProductRequest2() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        JsonSerializer<UUID> keySerializer = new JsonSerializer<>();
        when(producerFactory.createProducer()).thenReturn(new MockProducer<>(true, keySerializer, new StringSerializer()));
        when(producerFactory.transactionCapable()).thenReturn(false);
        KafkaTemplate<UUID, String> anyTemplate = new KafkaTemplate<>(producerFactory);
        ProductRepository repository = mock(ProductRepository.class);
        when(repository.findAllById(Mockito.<Iterable<UUID>>any())).thenReturn(new ArrayList<>());
        StockEventListener stockEventListener = new StockEventListener(anyTemplate, new ProductServiceImpl(repository));

        // Act
        stockEventListener.listenProductRequest("42", UUID.randomUUID());

        // Assert
        verify(repository).findAllById(isA(Iterable.class));
        verify(producerFactory).createProducer();
        verify(producerFactory).transactionCapable();
    }

    /**
     * Method under test:
     * {@link StockEventListener#listenProductRequest(String, UUID)}
     */
    @Test
    void testListenProductRequest3() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        UUIDSerializer keySerializer = new UUIDSerializer();
        when(producerFactory.createProducer()).thenReturn(new MockProducer<>(true, keySerializer, new JsonSerializer<>()));
        when(producerFactory.transactionCapable()).thenReturn(false);
        KafkaTemplate<UUID, String> anyTemplate = new KafkaTemplate<>(producerFactory);
        ProductRepository repository = mock(ProductRepository.class);
        when(repository.findAllById(Mockito.<Iterable<UUID>>any())).thenReturn(new ArrayList<>());
        StockEventListener stockEventListener = new StockEventListener(anyTemplate, new ProductServiceImpl(repository));

        // Act
        stockEventListener.listenProductRequest("42", UUID.randomUUID());

        // Assert
        verify(repository).findAllById(isA(Iterable.class));
        verify(producerFactory).createProducer();
        verify(producerFactory).transactionCapable();
    }
    /**
     * Method under test:
     * {@link StockEventListener#listenProductRequest(String, UUID)}
     */
    @Test
    void testListenProductRequest_multi() {
        // Arrange
        ProducerFactory<UUID, String> producerFactory = mock(ProducerFactory.class);
        UUIDSerializer keySerializer = new UUIDSerializer();
        when(producerFactory.createProducer()).thenReturn(new MockProducer<>(true, keySerializer, new JsonSerializer<>()));
        when(producerFactory.transactionCapable()).thenReturn(false);
        KafkaTemplate<UUID, String> anyTemplate = new KafkaTemplate<>(producerFactory);
        ProductRepository repository = mock(ProductRepository.class);
        when(repository.findAllById(Mockito.<Iterable<UUID>>any())).thenReturn(new ArrayList<>());
        StockEventListener stockEventListener = new StockEventListener(anyTemplate, new ProductServiceImpl(repository));

        // Act
        stockEventListener.listenProductRequest(List.of(UUID.randomUUID(), UUID.randomUUID()).toString(), UUID.randomUUID());

        // Assert
        verify(repository).findAllById(isA(Iterable.class));
        verify(producerFactory).createProducer();
        verify(producerFactory).transactionCapable();
    }
}
