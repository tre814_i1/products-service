package fr.epsi.rennes.mspr.tpre814.products.api;

import fr.epsi.rennes.mspr.tpre814.products.errors.NotActiveException;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.listener.KafkaBackoffException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.NoSuchElementException;

/**
 * Magic Rest API error handler
 */
@Slf4j
@RestControllerAdvice
public class ErrorHandler {

    @ExceptionHandler({NoSuchElementException.class, EntityNotFoundException.class,
            NotActiveException.class})
    public ResponseEntity<String> handleNotFoundException(RuntimeException e) {
        log.error("", e);
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleValidationError(IllegalArgumentException e) {
        log.error("Error Validating Customer", e);
        return ResponseEntity.badRequest().body(e.getMessage());
    }

    @ExceptionHandler({EnumConstantNotPresentException.class})
    public ResponseEntity<String> handleEnumConstantNotPresentException(EnumConstantNotPresentException e) {
        log.error("Something went wrong !", e);
        return ResponseEntity.badRequest().body("Invalid state : " + e.constantName());
    }

    @ExceptionHandler({Exception.class, NullPointerException.class})
    public ResponseEntity<String> handleException(Exception e) {
        log.error("Internal server error", e);
        return ResponseEntity.internalServerError().body("Internal server error");
    }

    @ExceptionHandler(KafkaBackoffException.class)
    public ResponseEntity<String> handleKafkaBackoffException(KafkaBackoffException e) {
        log.error("Service Unavailable", e);
        return ResponseEntity.status(503).body("Service Unavailable %s".formatted(e.getMessage()));
    }
}
