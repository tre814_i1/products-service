package fr.epsi.rennes.mspr.tpre814.products.utils;

import fr.epsi.rennes.mspr.tpre814.products.database.entity.ProductEntity;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static fr.epsi.rennes.mspr.tpre814.models.Stubs.newTestProductEntity;
import static org.junit.jupiter.api.Assertions.*;

class ProductMapperUnitTest {
    /**
     * Method under test: {@link ProductsMapper#toProduct(ProductEntity)}
     */
    @Test
    void testToProducts_noStock() {
        // Arrange and Act
        ProductEntity entity = new ProductEntity();
        entity.setId(UUID.randomUUID());
        entity.setName("Test");
        entity.setPrice(10.0);
        entity.setDescription("Test description");
        System.out.println(entity);

        String err = assertThrows(NullPointerException.class, () -> ProductsMapper.toProduct(entity)).getMessage();

        // Assert
        assertTrue(err.contains(".getStock()\" is null"));

    }

    /**
     * Method under test: {@link ProductsMapper#toProduct(ProductEntity)}
     */
    @Test
    void testToProducts_noName() {
        // Arrange
        ProductEntity entity = new ProductEntity();
        entity.setStock(10);
        entity.setPrice(10.0);
        entity.setDescription("Test description");
        System.out.println(entity);
        // Act
        Product actualResult = ProductsMapper.toProduct(entity);

        // Assert
        assertEquals(10, actualResult.getStock());
        assertEquals(10.0, actualResult.getDetails().getPrice());
        assertNull(actualResult.getName());
        assertEquals("Test description", actualResult.getDetails().getDescription());
        assertNull(actualResult.getId());
        assertNull(actualResult.getDetails().getName());
    }


    /**
     * Method under test: {@link ProductsMapper#toProduct(ProductEntity)} (ProductEntity)}
     */
    @Test
    void testToProductEntity_noPrice() {
        // Arrange
        ProductEntity entity = newTestProductEntity();

        // Act
        assertThrows(NullPointerException.class, () -> entity.setPrice(null));
    }

    /**
     * Method under test: {@link ProductsMapper#toProduct(ProductEntity)}
     */
    @Test
    void testToProductEntity_noStock() {
        // Arrange
        ProductEntity entity = newTestProductEntity();

        // Act
        assertThrows(NullPointerException.class, () -> entity.setStock(null));
    }

}
