package fr.epsi.rennes.mspr.tpre814.products.services.interfaces;

import fr.epsi.rennes.mspr.tpre814.products.api.interfaces.CRUDInterface;
import fr.epsi.rennes.mspr.tpre814.products.database.entity.ProductEntity;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockRequest;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;

import java.util.List;
import java.util.UUID;

public interface ProductService extends CRUDInterface<ProductEntity, UUID, Product> {
    List<Product> createAll(List<ProductEntity> product);

    void handleStockRequest(List<ProductEntity> entities, StockRequest request);

    List<ProductEntity> findAllById(List<UUID> list);
}
