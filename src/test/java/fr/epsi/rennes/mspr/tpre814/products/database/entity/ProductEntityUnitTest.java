package fr.epsi.rennes.mspr.tpre814.products.database.entity;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class ProductEntityUnitTest {
    /**
     * Method under test: {@link ProductEntity#toString()}
     */
    @Test
    void testToString() {
        // Arrange, Act and Assert
        assertEquals("ProductEntity(isNew=false, id=null, name=null, description=null, stock=null, price=null, active=false)", (new ProductEntity()).toString());
    }

    /**
     * Method under test: {@link ProductEntity#toString()}
     */
    @Test
    void testToString2() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        productEntity.setName("Name");
        productEntity.setId(null);

        // Act and Assert
        assertEquals("ProductEntity(isNew=false, id=null, name=Name, description=null, stock=1, price=10.0, active=false)", productEntity.toString());
    }

    @Test
    void testValidate_new_ok() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        productEntity.setName("Name");
        productEntity.setId(null);
        productEntity.setDescription("Description");
        productEntity.setNew(true);
        // Act and Assert
        productEntity.validate();
    }

    @Test
    void testValidate_idNotNull_new_nok() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        productEntity.setName("Name");
        productEntity.setId(java.util.UUID.randomUUID());
        productEntity.setDescription("Description");
        productEntity.setNew(true);
        // Act and Assert
        String err = assertThrows(IllegalStateException.class, productEntity::validate).getMessage();
        assertEquals("Product id must be null", err);
    }

    @Test
    void testValidate_idNotNull_notNew_ok() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        productEntity.setName("Name");
        productEntity.setId(UUID.randomUUID());
        productEntity.setDescription("Description");
        productEntity.setNew(false);
        // Act and Assert
        assertDoesNotThrow(() -> productEntity.validate());
    }

    @Test
    void testValidate_nameEmpty_nok() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        productEntity.setName("");
        productEntity.setId(null);
        productEntity.setDescription("Description");
        productEntity.setNew(true);
        // Act and Assert
        try {
            productEntity.validate();
        } catch (IllegalArgumentException e) {
            assertEquals("Name is required", e.getMessage());
        }
    }

    @Test
    void testValidate_descriptionEmpty_nok() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setPrice(10.0d);
        productEntity.setStock(1);
        productEntity.setName("Name");
        productEntity.setId(null);
        productEntity.setDescription("");
        productEntity.setNew(true);
        // Act and Assert
        try {
            productEntity.validate();
        } catch (IllegalArgumentException e) {
            assertEquals("Description is required", e.getMessage());
        }
    }

    @Test
    void testValidate_stockNegative_nok() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setPrice(10.0d);
        productEntity.setStock(-1);
        productEntity.setName("Name");
        productEntity.setId(null);
        productEntity.setDescription("Description");
        productEntity.setNew(true);
        // Act and Assert
        String err = assertThrows(IllegalArgumentException.class, productEntity::validate).getMessage();
        assertEquals("Stock must be greater than or equal to 0", err);
    }

    @Test
    void testValidate_priceNegative_nok() {
        // Arrange
        ProductEntity productEntity = new ProductEntity();
        productEntity.setPrice(-1.0d);
        productEntity.setStock(1);
        productEntity.setName("Name");
        productEntity.setId(null);
        productEntity.setDescription("Description");
        productEntity.setNew(true);
        // Act and Assert
        try {
            productEntity.validate();
        } catch (IllegalArgumentException e) {
            assertEquals("Price must be greater than or equal to 0", e.getMessage());
        }
    }
    @Test
    void testAllArgsConstructor() {
        // Arrange, Act and Assert
        assertDoesNotThrow(() -> new ProductEntity(true, UUID.randomUUID(), "Name", "Description", 1, 10.0d, true));
    }
    @Test
    void testNoArgsConstructor() {
        // Arrange, Act and Assert
        assertDoesNotThrow(() -> new ProductEntity());
    }
    @Test
    void testBuilder_ok() {
        // Arrange
        ProductEntity.ProductEntityBuilder productEntity = ProductEntity.builder()
                .id(UUID.randomUUID())
                .name("Name")
                .description("Description")
                .stock(1)
                .price(10.0d);
        // Act and Assert
        assertDoesNotThrow(productEntity::build);
    }
    @Test
    void testBuilder() {
        // Arrange, Act and Assert
        assertThrows(NullPointerException.class, () -> ProductEntity.builder().build());
    }
}
