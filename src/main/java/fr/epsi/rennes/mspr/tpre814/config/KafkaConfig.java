package fr.epsi.rennes.mspr.tpre814.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;


@EnableKafka
@Configuration
public class KafkaConfig {
    /**
     * Topics names definition
     */
    public static final String RESERVE_TOPIC = "stock-reserve";
    public static final String STOCK_RESPONSE = "stock-response";
    public static final String PRODUCTS_REQUEST = "request-products";
    public static final String CUSTOMER_REQUEST = "customer-request";
    public static final String CUSTOMER_RESPONSE = "customer-response";
    public static final String SERVICE_KAFKA_GROUP = "${spring.kafka.group-id}";

}
