package fr.epsi.rennes.mspr.tpre814.products.database.entity;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

@Data
@Entity(name = "Product")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductEntity implements Serializable {
    private static final @Serial long serialVersionUID = -7409786442773105329L;
    transient boolean isNew;

    @Id
    @Column(name = "id", updatable = false, nullable = false, unique = true)
    private UUID id;

    @Column
    private String name;
    @Column
    private String description;
    @Column
    @NonNull
    private Integer stock;
    @Column(name = "price", columnDefinition = "DECIMAL(15, 2)")
    @NonNull
    private Double price;
    @Column(columnDefinition = "BOOLEAN DEFAULT TRUE")
    private boolean active;

    public void validate() {
        if (isNew && this.getId() != null || !isNew && this.getId() == null) {
            throw new IllegalStateException("Product id must %s null".formatted(isNew ? "be" : "not be"));
        }
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Name is required");
        }
        if (description == null || description.isEmpty()) {
            throw new IllegalArgumentException("Description is required");
        }
        if (stock < 0) {
            throw new IllegalArgumentException("Stock must be greater than or equal to 0");
        }
        if (price < 0) {
            throw new IllegalArgumentException("Price must be greater than or equal to 0");
        }
    }
}

