package fr.epsi.rennes.mspr.tpre814.products.api;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RootControllerDiffblueTest {
    /**
     * Method under test: {@link RootController#ping()}
     */
    @Test
    void testPing() {
        // Arrange, Act and Assert
        assertEquals(HttpStatus.OK, (new RootController()).ping());
    }
}
