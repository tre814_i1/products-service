package fr.epsi.rennes.mspr.tpre814.products.api;

import fr.epsi.rennes.mspr.tpre814.products.api.interfaces.CRUDInterface;
import fr.epsi.rennes.mspr.tpre814.products.services.interfaces.ProductService;
import fr.epsi.rennes.mspr.tpre814.products.utils.ProductsMapper;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(value = "/api/v2/products")
@RequiredArgsConstructor
public class ProductController implements CRUDInterface<Product, UUID, Product> {

    private final ProductService productService;

    /**
     * {@inheritDoc}
     */
    @Override
    @PostMapping
    public Product create(@RequestBody @NonNull Product product) {
        log.info("Creating product: {}", product);
        return productService.create(ProductsMapper.toEntity(product));
    }

    /**
     * {@inheritDoc}
     */
    @PostMapping(value = {"/all"})
    public List<Product> createAll(@RequestBody List<Product> product) {
        log.info("Creating product: {}", product);
        return productService.createAll(ProductsMapper.toEntity(product));
    }

    /**
     * {@inheritDoc}
     */
    // TO BE REMOVED (tests before broker only)
    @Override
    @GetMapping(value = {"/all"})
    public List<Product> all() {
        return ProductsMapper.toProduct(productService.all());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Product update(Product entity) {
        return ProductsMapper.toProduct(productService.update(ProductsMapper.toEntity(entity)));
    }

    @PutMapping(value = {"/{id}"})
    public Product update(@PathVariable UUID id, @RequestBody @NonNull Product entity) {
        entity.setId(id);
        return update(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @DeleteMapping(value = {"/{productId}"})
    public void delete(@PathVariable UUID productId) {
        productService.delete(productId);
    }

    /**
     * {@inheritDoc}
     */
    @GetMapping(value = {"/{productId}"})
    public Product get(@PathVariable UUID productId) {
        return ProductsMapper.toProduct(productService.get(productId));
    }
}
