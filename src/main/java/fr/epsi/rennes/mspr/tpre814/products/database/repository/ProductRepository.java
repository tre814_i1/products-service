package fr.epsi.rennes.mspr.tpre814.products.database.repository;

import fr.epsi.rennes.mspr.tpre814.products.database.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, UUID> {
}