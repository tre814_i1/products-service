#!/usr/bin/env sh
# This script is used to build Image and push to docker hub

gradle clean bootBuildImage
if [ $? -eq 0 ]; then
  echo "Build successful"
  # requires docker login on current machine
  docker push mistayan/product-service:v0.4.4
  docker push mistayan/product-service:latest
else
  echo "Build failed"
fi

# wait user input to exit
read -p "Press enter to exit"
