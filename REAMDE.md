# Multi-Service Application PaieTonKawa : Product Service
Auteurs :
- Mistayan - [Gitlab](https://gitlab.com/Mistayan/)

Contributeurs :
- Bob-117 - [Gitlab](https://gitlab.com/Bob-117/)
- Sullfurick - [Gitlab](https://gitlab.com/Sullfurick)


Développé avec :
- Java 21+
- SpringBoot 3.2.5+
- Kafka 2.2.7+
- Gradle 8.7+
- PostgreSQL 16.3+
- Docker latest

____

[TOC]

____

## Product Service
   
### Description

Ce projet est un exemple d'application multi-services utilisant Spring Boot et Spring Cloud. Il est composé de 3 services :
- order-service : Service de gestion des commandes
- product-service : Service de gestion des produits
- Customer-service : Service de gestion des clients


### Architecture

```mermaid
graph v
    subgraph middlewares
        I[API Gateway] <--> S[Keycloak]
    end
    subgraph frontend
        K[Frontend]
        K <--> I
    end
    subgraph micro-services
        A[Product Service]
        C[Customer Service]
        B[Order Service]
        I <--> A[Product Service]
        I <--> C[Customer Service]
        I <--> B[Order Service]
        subgraph data
            D[Postgres]
            E[Postgres]
            F[Postgres]
        end
        subgraph broker
            Y[Kafka Broker] <--> Z[Zookeeper]
        end
        D <--> A <--> Y 
        E <--> B <--> Y
        F <--> C <--> Y
    end
```

### Installation

1. Cloner le projet
    ```shell
    git clone https://gitlab.com/tre814_i1/product-service.git
    ```
   
2. Se rendre dans le dossier du projet
    ```shell
    cd product-service
    ```
   
3. Lancer la commande `./gradlew bootBuildImage` pour construire le programme en Image:
   cette étape est optionnelle, les images existent déjà sur un repository en ligne, PUBLIC 

   
4. Créer un fichier .env a vous:
    ```shell
    cp .env.example .env
    nano .env
    ```
   
5. Puis `docker compose up -f ... -d` pour lancer les compose requis
    ```shell
    docker compose -f compose.kafka.yml up -d
    ```
   attendez quelques secondes, puis 
    ```shell
    docker compose -f compose.app_product.yml up -d
    ```

### Endpoints

- GET /products : Récupérer la liste des clients
- POST /products : Créer un client
- GET /products/{id} : Récupérer un client
- PUT /products/{id} : Mettre à jour un client
- DELETE /products/{id} : Supprimer un client

## Fonctionnement de l'application

#### Collection Postman
- [Collection Postman](https://api.postman.com/collections/24975391-32fbb431-8db9-4e0b-bc81-92dfab6498cc?access_key=PMAT-01J0DNVA4R7F761V4C9N9XCJ6R)
#### Uses Cases
- Créer un client
```mermaid
sequenceDiagram
    Frontend ->> API Gateway: POST /products
    alt authenticated ? 
    API Gateway ->> Frontend: 401 Unauthorized
    else
    API Gateway ->> Product Service: POST /products
        Product Service ->> Postgres: INSERT INTO products
    end
    Postgres ->> Product Service: Product saved
    Product Service ->> API Gateway: 200 Created
    API Gateway ->> Frontend: 200 Created

```
- Récupérer un produit
```mermaid
sequenceDiagram
   Frontend ->> API Gateway: GET /products/{id}

   API Gateway ->> Product Service: GET /products/{id}

   Product Service ->> Postgres: findProductById({id})
   Postgres ->> Product Service: Product


   Product Service ->> API Gateway: 200 OK [Product]
   API Gateway ->> Frontend: 200 OK [Product]
```
- Mettre à jour un produit
```mermaid
sequenceDiagram
    Frontend ->> API Gateway: PUT /products/{id}
    alt authenticated ? 
    API Gateway ->> Frontend: 401 Unauthorized
    else
    API Gateway ->> Product Service: PUT /products/{id}
        Product Service ->> Postgres: UPDATE products
    end
    Postgres ->> Product Service: Product updated
    Product Service ->> API Gateway: 200 OK
    API Gateway ->> Frontend: 200 OK
```
- Supprimer un produit
```mermaid
sequenceDiagram
    Frontend ->> API Gateway: DELETE /products/{id}
    alt authenticated ? 
    API Gateway ->> Frontend: 401 Unauthorized
    else
    API Gateway ->> Product Service: DELETE /products/{id}
        Product Service ->> Postgres: DELETE products
    end
    Postgres ->> Product Service: Product deleted (disabled)
    Product Service ->> API Gateway: 200 OK
    API Gateway ->> Frontend: 200 OK
```
- Récupérer la liste des produits
```mermaid
sequenceDiagram
    Frontend ->> API Gateway: GET /products
    API Gateway ->> Product Service: GET /products
    Product Service ->> Postgres: findAllProducts()
    Postgres ->> Product Service: Products
    Product Service ->> API Gateway: 200 OK [Products]
    API Gateway ->> Frontend: 200 OK [Products]
```

____

#### Exemples d'utilisation :

- voir la collection Postman
- si l'application est lancée :
  - [http://localhost:8080/products](http://localhost:8080/products) : liste des produits
  - [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html) : documentation de l'API
  
...

