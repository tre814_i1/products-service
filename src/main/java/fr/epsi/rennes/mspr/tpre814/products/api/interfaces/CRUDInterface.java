package fr.epsi.rennes.mspr.tpre814.products.api.interfaces;

import java.util.List;
import java.util.NoSuchElementException;

public interface CRUDInterface<T, K, R> {

    /**
     * Create a new entity in the application
     *
     * @param entity the entity to create
     * @return the created entity
     * @throws IllegalArgumentException if the entity is invalid
     */
    R create(T entity);

    /**
     * Get an entity by its id
     *
     * @param id the id of the entity
     * @return the entity
     * @throws NoSuchElementException if the entity is not found
     */
    T get(K id);

    /**
     * Get all entities
     *
     * @return a list of all entities
     */
    List<T> all();

    /**
     * Update an entity
     *
     * @param entity the entity to update (must have a valid id)
     * @return the updated entity
     * @throws NoSuchElementException if the entity is not found
     */
    T update(T entity);

    /**
     * Delete an entity by its id
     *
     * @param id the id of the entity to delete
     * @throws NoSuchElementException if the entity is not found
     */
    void delete(K id);
}
