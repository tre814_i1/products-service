package fr.epsi.rennes.mspr.tpre814.products.services;

import fr.epsi.rennes.mspr.tpre814.products.database.entity.ProductEntity;
import fr.epsi.rennes.mspr.tpre814.products.database.repository.ProductRepository;
import fr.epsi.rennes.mspr.tpre814.products.services.interfaces.ProductService;
import fr.epsi.rennes.mspr.tpre814.products.utils.ProductsMapper;
import fr.epsi.rennes.mspr.tpre814.shared.events.ProductRequest;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockOperation;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockRequest;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SoftAssertionsExtension.class)
class ProductServiceImplTest {

    ProductRepository repo;
    ProductService service;

    ProductEntity entity;
    @BeforeEach
    void setUp() {
        repo = mock(ProductRepository.class);
        service = new ProductServiceImpl(repo);
        entity = new ProductEntity();
    }

    @Test
    void create() {
        // Arrange
        entity.setId(null);
        entity.setName("Test");
        entity.setPrice(10.0);
        entity.setStock(10);
        entity.setDescription("Test description");

        when(repo.save(entity)).thenReturn(entity);
        when(repo.findById(entity.getId())).thenReturn(java.util.Optional.of(entity));

        // Act
        Product actualCreateResult = service.create(entity);

        // Assert
        assertEquals(10, actualCreateResult.getStock());
        assertEquals(10.0, actualCreateResult.getDetails().getPrice());
        assertEquals("Test", actualCreateResult.getName());
        assertEquals("Test description", actualCreateResult.getDetails().getDescription());
        assertNotNull(actualCreateResult.getId());
    }
    @Test
    void create_withId_nok() {
        // Arrange
        entity.setId(UUID.randomUUID());
        entity.setName("Test");
        entity.setPrice(10.0);
        entity.setStock(10);
        entity.setDescription("Test description");

        // Act / Assert
        assertThrows(IllegalStateException.class, () -> service.create(entity));
    }
    @Test
    void createAll() {
        entity.setId(null);
        entity.setName("Test");
        entity.setPrice(10.0);
        entity.setStock(10);
        entity.setDescription("Test description");

        when(repo.save(entity)).thenReturn(entity);
        when(repo.findById(entity.getId())).thenReturn(java.util.Optional.of(entity));

        // Act
        Product actualCreateResult = service.create(entity);

        // Assert
        assertEquals(10, actualCreateResult.getStock());
        assertEquals(10.0, actualCreateResult.getDetails().getPrice());
        assertEquals("Test", actualCreateResult.getName());
        assertEquals("Test description", actualCreateResult.getDetails().getDescription());
        assertNotNull(actualCreateResult.getId());
    }

    @Test
    void get() {
        // Arrange
        entity.setId(UUID.randomUUID());
        entity.setName("Test");
        entity.setPrice(10.0);
        entity.setStock(10);
        entity.setActive(true);
        entity.setDescription("Test description");

        when(repo.findById(entity.getId())).thenReturn(java.util.Optional.of(entity));

        // Act
        ProductEntity actualGetResult = service.get(entity.getId());

        // Assert
        assertEquals(10, actualGetResult.getStock());
        assertEquals(10.0, actualGetResult.getPrice());
        assertEquals("Test", actualGetResult.getName());
        assertEquals("Test description", actualGetResult.getDescription());
        assertNotNull(actualGetResult.getId());
    }

    @Test
    void all() {
        // Arrange
        entity.setId(UUID.randomUUID());
        entity.setName("Test");
        entity.setPrice(10.0);
        entity.setStock(10);
        entity.setDescription("Test description");

        when(repo.findAll()).thenReturn(java.util.List.of(entity));

        // Act
        ProductEntity actualGetResult = service.all().getFirst();

        // Assert
        assertEquals(10, actualGetResult.getStock());
        assertEquals(10.0, actualGetResult.getPrice());
        assertEquals("Test", actualGetResult.getName());
        assertEquals("Test description", actualGetResult.getDescription());
        assertNotNull(actualGetResult.getId());
    }

    @Test
    void update() {
        // Arrange
        entity.setId(UUID.randomUUID());
        entity.setName("Test");
        entity.setPrice(10.0);
        entity.setStock(10);
        entity.setActive(true);
        entity.setDescription("Test description");

        when(repo.save(entity)).thenReturn(entity);
        when(repo.findById(entity.getId())).thenReturn(java.util.Optional.of(entity));

        // Act
        ProductEntity actualUpdateResult = service.update(entity);

        // Assert
        assertEquals(10, actualUpdateResult.getStock());
        assertEquals(10.0, actualUpdateResult.getPrice());
        assertEquals("Test", actualUpdateResult.getName());
        assertEquals("Test description", actualUpdateResult.getDescription());
        assertNotNull(actualUpdateResult.getId());
    }

    @Test
    @Disabled("Not implemented")
    void delete() {

    }

    @Test
    void handleStockRequest_sub() {
        entity.setId(UUID.randomUUID());
        entity.setName("Test");
        entity.setPrice(10.0);
        entity.setActive(true);
        entity.setStock(10);
        entity.setDescription("Test description");

        when(repo.findById(entity.getId())).thenReturn(java.util.Optional.of(entity));

        ProductRequest requestedProduct = new ProductRequest(entity.getId(), 10);
        StockRequest request = new StockRequest();
        request.setOp(StockOperation.SUB);
        request.setPayload(java.util.List.of(requestedProduct));

        when(repo.save(entity)).then(invocation -> {
            entity.setStock(0);
            return entity;
        });
        when(repo.findById(entity.getId())).thenReturn(java.util.Optional.of(entity));
        // Act
        int actualStock = service.get(entity.getId()).getStock();
        service.handleStockRequest(java.util.List.of(entity), request);
        int actualStockAfter = service.get(entity.getId()).getStock();

        // Assert
        assertEquals(10, actualStock);
        assertEquals(0, actualStockAfter);
    }
    @Test
    void handleStockRequest_add() {
        entity.setId(UUID.randomUUID());
        entity.setName("Test");
        entity.setPrice(10.0);
        entity.setStock(10);
        entity.setActive(true);
        entity.setDescription("Test description");

        when(repo.findById(entity.getId())).thenReturn(java.util.Optional.of(entity));

        ProductRequest requestedProduct = new ProductRequest(entity.getId(), 10);
        StockRequest request = new StockRequest();
        request.setOp(StockOperation.ADD);
        request.setPayload(java.util.List.of(requestedProduct));

        when(repo.save(entity)).then(invocation -> {
            entity.setStock(20);
            return entity;
        });
        when(repo.findById(entity.getId())).thenReturn(java.util.Optional.of(entity));
        // Act
        int actualStock = service.get(entity.getId()).getStock();
        service.handleStockRequest(java.util.List.of(entity), request);
        int actualStockAfter = service.get(entity.getId()).getStock();

        // Assert
        assertEquals(10, actualStock);
        assertEquals(20, actualStockAfter);
    }

    @Test
    void findAllById() {
        // Arrange
        entity.setId(UUID.randomUUID());
        entity.setName("Test");
        entity.setPrice(10.0);
        entity.setStock(10);
        entity.setDescription("Test description");

        when(repo.findAllById(java.util.List.of(entity.getId()))).thenReturn(java.util.List.of(entity));

        // Act
        List<Product> actualGetResult = ProductsMapper.toProduct(service.findAllById(java.util.List.of(entity.getId())));

        // Assert
        Product actualProduct = actualGetResult.getFirst();
        assertEquals(10, actualProduct.getStock());
        assertEquals(10.0, actualProduct.getDetails().getPrice());
        assertEquals("Test", actualProduct.getName());
        assertEquals("Test description", actualProduct.getDetails().getDescription());
        assertNotNull(actualProduct.getId());


    }
}