package fr.epsi.rennes.mspr.tpre814.products.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epsi.rennes.mspr.tpre814.products.database.entity.ProductEntity;
import fr.epsi.rennes.mspr.tpre814.products.services.interfaces.ProductService;
import fr.epsi.rennes.mspr.tpre814.products.utils.ProductsMapper;
import fr.epsi.rennes.mspr.tpre814.shared.events.ProductRequest;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockRequest;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockResponse;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static fr.epsi.rennes.mspr.tpre814.config.KafkaConfig.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class StockEventListener {
    private static final ObjectMapper mapper = new ObjectMapper();
    private final KafkaTemplate<UUID, String> anyTemplate;
    private final ProductService service;

    /**
     * Parse a Json request to a Java Object
     *
     * @param req   the request to parse
     * @param clazz the class of the object to parse
     * @return the parsed object
     * @throws RuntimeException if the request cannot be parsed
     */
    private static Object getRequest(String req, Class<?> clazz) {
        log.debug("Parsing request");
        try {
            return mapper.readValue(req, clazz);
        } catch (Exception e) {
            throw new RuntimeException("Cannot parse request: " + req, e);
        }
    }


    /**
     * Wrapper Function to find all products by their id
     *
     * @param requestedProducts the list of requested products
     * @return the list of products found (if any)
     */
    private List<ProductEntity> findAllById(List<ProductRequest> requestedProducts) {
        log.info("Finding products by id : {}", requestedProducts);
        return service.findAllById(
                requestedProducts.stream()
                        .map(ProductRequest::id)
                        .toList()
        );
    }

    //##############################################################################################################
    //################################################ LISTENERS ##################################################
    //##############################################################################################################


    /**
     * Await for other service to request stock operations
     *
     * @param req the request to handle, in Json format
     * @param key the key of the request
     */
    @KafkaListener(topics = RESERVE_TOPIC, groupId = "product-service-1",
            autoStartup = "true", info = "Product Service Listener")
    public void listenStockEvents(@NonNull String req,  // StockRequest.toJson()
                                  @Header(KafkaHeaders.RECEIVED_KEY) UUID key
    ) {
        log.info("Received new Products request: " + key + "  ===>  " + req);
        // Get the requested products from the input Json string

        StockRequest request = (StockRequest) getRequest(req.replace("products", "payload"), StockRequest.class);

        // Get products and apply the requested operation
        List<ProductEntity> entities = findAllById(request.getPayload());
        List<Product> products = entities.stream()
                .map(ProductsMapper::toProduct)
                .toList();
        log.info("Products found: " + products);
        service.handleStockRequest(entities, request);

        // response to the order-service
        StockResponse stockResponse = new StockResponse(request.getId(), products);
        log.info("Sending response: " + stockResponse);
        anyTemplate.send(STOCK_RESPONSE, request.getId(), stockResponse.toJson());
    }

    @KafkaListener(topics = PRODUCTS_REQUEST, groupId = SERVICE_KAFKA_GROUP,
            autoStartup = "true", info = "Product info Listener")
    public void listenProductRequest(@NonNull String req, // List[UUID]
                                     @Header(KafkaHeaders.RECEIVED_KEY) UUID key) {
        log.info("Received new ProductsDetails request: " + key + "  ===>  " + req);
        // req is a list of product ids
        List<UUID> requested = parseIds(req);
        List<ProductEntity> entities = service.findAllById(requested);
        List<Product> products = entities.stream()
                .map(ProductsMapper::toProduct)
                .toList();
        StockResponse stockResponse = new StockResponse(key, products);
        anyTemplate.send(STOCK_RESPONSE, key, stockResponse.toJson());
    }

    private List<UUID> parseIds(String req) {
        return req.substring(1, req.length() - 1).isEmpty() ?
                List.of() :
                Stream.of(req.substring(1, req.length() - 1).split(","))
                        .filter(s -> !s.isBlank())
                        .map(String::trim)
                        .map(String::strip)
                        .map(UUID::fromString)
                        .toList();
    }
}