package fr.epsi.rennes.mspr.tpre814.products.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v2/")
public class RootController {

    @RequestMapping("/ping")
    public HttpStatus ping() {
        return HttpStatus.OK;
    }
}
