package fr.epsi.rennes.mspr.tpre814.products.utils;

import fr.epsi.rennes.mspr.tpre814.products.database.entity.ProductEntity;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import fr.epsi.rennes.mspr.tpre814.shared.models.ProductDetails;
import fr.epsi.rennes.mspr.tpre814.shared.utils.ModelMapper;

import java.util.List;

public class ProductsMapper implements ModelMapper {
    public static Product toProduct(ProductEntity entity) {
        Product product = new Product();
        product.setId(entity.getId());
        product.setName(entity.getName());
        product.setStock(entity.getStock());
        ProductDetails details = new ProductDetails();
        details.setPrice(entity.getPrice());
        details.setName(entity.getName());
        details.setDescription(entity.getDescription());
        product.setDetails(details);
        return product;
    }


    public static List<Product> toProduct(List<ProductEntity> all) {
        return all.stream().map(ProductsMapper::toProduct).toList();
    }

    public static List<ProductEntity> toEntity(List<Product> product) {
        return product.stream().map(ProductsMapper::toEntity).toList();
    }

    public static ProductEntity toEntity(Product product) {
        ProductEntity entity = new ProductEntity();
        entity.setId(product.getId());
        entity.setName(product.getName());
        entity.setStock(product.getStock());
        entity.setPrice(product.getDetails().getPrice());
        entity.setDescription(product.getDetails().getDescription());
        return entity;
    }
}
